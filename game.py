import random
import discord


class Song:
    def __init__(self, title, url):
        self.title = title
        self.url = url


class TeamMember:
    def __init__(self, member: discord.Member):
        self.member = member
        self.songs = []
        self.unused_songs = []
        self.current_song = None

    def add_song(self, song):
        self.songs.append(song)
        self.unused_songs.append(song)


class Team:
    def __init__(self, team_members):
        self.team_members = team_members
        self.score = 0
        self.team_name = 'undefined'


class TooManyTeamsError(Exception):
    pass


class Game:
    def __init__(self, num_of_teams, all_members):
        if num_of_teams > len(all_members) / 2:
            raise TooManyTeamsError

        self.chain_size = len(all_members) // num_of_teams
        remainder = len(all_members) % num_of_teams

        self.teams = []
        self.all_members = all_members.copy()

        for i in range(num_of_teams):
            team_members = random.sample(all_members,
                                         self.chain_size + 1 if i < remainder
                                         else self.chain_size)
            self.teams.append(Team(team_members))
            for m in team_members:
                all_members.remove(m)

        self.rounds_played = 0
        self.rounds_per_game = self.chain_size + 1 if remainder != 0 \
            else self.chain_size

    def next_round(self):
        for team in self.teams:
            # Rotate for next round.
            team_members = team.team_members
            team_members.append(team_members[0])
            team_members.pop(0)

            guesser = team_members[-1]
            if not guesser.unused_songs:
                guesser.unused_songs = guesser.songs.copy()
            guesser.current_song = random.choice(guesser.unused_songs)
            guesser.unused_songs.remove(guesser.current_song)

        self.rounds_played += 1
        return self.teams

    def get_current_chain(self, team_num):
        return self.teams[team_num].team_members[-self.chain_size:]

    def add_to_score(self, team_num, points):
        if points < 0 or points > self.chain_size - 1:
            raise ValueError
        self.teams[team_num].score += points

    def get_leaderboard(self):
        return sorted(self.teams, key=lambda team: team.score, reverse=True)
