# Discord Musical Whispers

Discord bot for playing Musical Whispers uwu

(Documentation may or may not happen, depending on whether I think it's worth it... so probably not)

## Setup
The bot needs `python-dotenv` and `discord.py` to run.
Type these commands into your terminal to install them if you don't already have them:
```bash
python -m pip install python-dotenv
python -m pip install -U discord.py[voice]
```
You also need to have **ffmpeg** installed.

You also need to add a .env file. Make a new file called `.env` in the same directory as `musical_whispers_bot.py`, and add the following line:
```
DISCORD_TOKEN=<your discord bot token here>
```