import asyncio
import json
import os
import random
import traceback
from dotenv import load_dotenv
import discord
from discord.ext import commands
import game


class GuildGame:
    def __init__(self, guild, show_instr=True):
        self.guild = guild
        self.reset_vars(show_instr)

    def reset_vars(self, show_instr=True):
        """
        Game states:
        0 - idle
        1 - team creation
        2 - team names
        3 - song inputs
        4 - instructions
        5 - game proper: listening
        6/6.5 - game proper: singing
        7 - game proper: guessing
        8 - game proper: score entering
        9 - game end (not implemented)
        """
        self.state = 0
        self.the_game = None
        self.role = None
        self.erplgd_channel = None
        self.top_secret_channel = None
        self.v_channel = None
        self.voice = None
        self.msg_with_reaction = None
        self.members_ready = []
        self.current_round_teams = None
        self.current_team_num = -1
        self.current_chain = []
        self.show_instr = show_instr


version = '1.1.2'
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
bot = commands.Bot(command_prefix='$')
guild_games = []
try:
    with open('players.json') as reader:
        all_player_data = json.load(reader)
except FileNotFoundError:
    all_player_data = {}

# ***EDIT THIS BASED ON HOW MANY .mp3 FILES YOU HAVE AS OPENING TRACKS***
num_of_openings = 5


def update_json():
    with open('players.json', 'w') as writer:
        json.dump(all_player_data, writer)


async def start_and_wait(ctx, show_instr=True):
    if ctx.guild is not None:
        # TODO Check if v_state has a channel
        v_state = ctx.author.voice
        if not v_state:
            await ctx.send('You\'re not in a voice channel.')
            return

        gg = discord.utils.get(guild_games, guild=ctx.guild)
        if not gg:
            gg = GuildGame(ctx.guild)
            guild_games.append(gg)

        gg.reset_vars(show_instr)

        gg.v_channel = v_state.channel

        gg.voice = discord.utils.get(bot.voice_clients, guild=ctx.guild)
        if not gg.voice:
            gg.voice = await gg.v_channel.connect()
        elif gg.voice.channel != gg.v_channel:
            await gg.voice.disconnect()
            gg.voice = await gg.v_channel.connect()

        num = random.randint(1, num_of_openings)
        gg.voice.stop()
        gg.voice.play(discord.FFmpegPCMAudio(f'sounds/opening{num}.mp3'))

        gg.role = discord.utils.get(ctx.guild.roles, name='listener')
        if not gg.role:
            await ctx.send(
                'To use the Musical Whispers bot, please ensure that '
                'this server has a role called \'*listener*\', and that the '
                'role is placed below the \'*Musical Whispers*\' role.\n\n'
                'Server admin, get on it!'
            )
            return

        gg.erplgd_channel = discord.utils.get(ctx.guild.voice_channels,
                                              name='Earplugged 🎧')
        if not gg.erplgd_channel:
            if gg.v_channel.category:
                erplgd_channel = \
                    await gg.v_channel.category.create_voice_channel(
                        'Earplugged 🎧'
                    )
            else:
                gg.erplgd_channel = \
                    await ctx.guild.create_voice_channel('Earplugged 🎧')

        gg.top_secret_channel = discord.utils.get(ctx.guild.text_channels,
                                                  name='top-secret-channel')
        if not gg.top_secret_channel:
            overwrites = {
                ctx.guild.default_role:
                    discord.PermissionOverwrite(read_messages=False),
                ctx.guild.me:
                    discord.PermissionOverwrite(read_messages=False),
                gg.role: discord.PermissionOverwrite(read_messages=True)
            }

            if ctx.channel.category:
                gg.top_secret_channel = \
                    await ctx.channel.category.create_text_channel(
                        'top-secret-channel', overwrites=overwrites
                    )
            else:
                gg.top_secret_channel = \
                    await ctx.guild.create_text_channel(
                        'top-secret-channel', overwrites=overwrites
                    )

        await ctx.send(f'Welcome to Musical Whispers! (Version {version})')
        await ctx.send('How many teams?')

        def check(message):
            if any([message.content.startswith(cmd)
                    for cmd in ['$start', '$wise_start', '$abort']]):
                return True

            return message.channel == ctx.channel \
                and message.author != bot.user

        try:
            gg.state = 1
            success = False
            while not success:
                message = await bot.wait_for('message', check=check,
                                             timeout=300)
                if any([message.content.startswith(cmd)
                        for cmd in ['$start', '$wise_start', '$abort']]):
                    # Abort.
                    gg.reset_vars(show_instr)
                    return
                success = await create_teams(gg, message.channel,
                                             message.content)

            await ctx.send('Now, please enter the names of the teams, '
                           'separated by commas.')
            success = False
            while not success:
                message = await bot.wait_for('message', check=check,
                                             timeout=300)
                if any([message.content.startswith(cmd)
                        for cmd in ['$start', '$wise_start', '$abort']]):
                    # Abort.
                    gg.reset_vars(show_instr)
                    return
                success = await team_names_song_inputs(
                    gg, message.channel, message.content
                )
        except asyncio.TimeoutError:
            await ctx.send('Timed out: game will now abort 🥺')
            gg.reset_vars(True)


async def create_teams(gg, text_channel, response):
    try:
        num_of_teams = int(response)
        if num_of_teams <= 0:
            raise ValueError

        await text_channel.send('Now creating the teams...')
        members = gg.v_channel.members
        # TODO Remove bots
        members.remove(bot.user)
        all_members = [game.TeamMember(member) for member in members]
        print(', '.join([m.member.name for m in all_members]))

        gg.the_game = game.Game(num_of_teams, all_members)

        await text_channel.send('Here are the teams:')
        for i, team in enumerate(gg.the_game.teams):
            str_of_names = ', '.join(
                [team_member.member.nick if team_member.member.nick
                    else team_member.member.name
                    for team_member in team.team_members]
            )
            await text_channel.send(f'Team {i+1}: {str_of_names}')

        gg.state = 2
        # Return successful.
        return True
    except (TypeError, ValueError):
        print(traceback.format_exc())
        await text_channel.send('You need to input a positive integer, bruv. '
                                'Try again.')
        # Return unsuccessful.
        return False
    except game.TooManyTeamsError:
        await text_channel.send('You\'ve entered too many teams, bruv. '
                                'Try again.')
        # Return unsuccessful.
        return False


async def team_names_song_inputs(gg, text_channel, response):
    team_names = response.split(',')
    if len(team_names) != len(gg.the_game.teams):
        await text_channel.send('Formatting error. Pls try again.')
        # Return unsuccessful.
        return False
    for i, team in enumerate(gg.the_game.teams):
        team.team_name = team_names[i].strip()

    await text_channel.send('Here are the teams:')
    for team in gg.the_game.teams:
        str_of_names = ', '.join(
            [team_member.member.nick if team_member.member.nick
                else team_member.member.name
                for team_member in team.team_members]
        )
        await text_channel.send(f'Team {team.team_name}: {str_of_names}')

    await text_channel.send(
        'I will now slide into each of your DMs. Please follow '
        'the instructions you receive to input your songs.'
    )
    for m in gg.the_game.all_members:
        await m.member.create_dm()
        await m.member.dm_channel.send(
            'To add a song, use the `$add` command.\n'
            'The response must be in the format '
            '<title (artist optional)> --- <url>\n'
            'For example: `$add Bad Romance (Lady Gaga) --- '
            'https://www.youtube.com/watch?v=qrO4YZeyl0I`'
        )
        await m.member.dm_channel.send(
            'You can add multiple songs at once using linebreaks '
            '(Shift + Enter). For example:\n'
            '`$add Bad Romance (Lady Gaga) --- '
            'https://www.youtube.com/watch?v=qrO4YZeyl0I\n'
            'Replay (Lady Gaga) --- '
            'https://www.youtube.com/watch?v=ZTwpCLZLdRs`'
        )
        await m.member.dm_channel.send(
            'You can also use the `$list`, `$remove` and `$edit` commands '
            'to change your song list until you\'re happy with it. '
            'For more information on how to use each of these commands, use '
            'the `$help` command, e.g. `$help list`.'
        )
    gg.msg_with_reaction = await text_channel.send(
        'Once you\'ve finished inputting your songs, click the 🐐 '
        'react. Once everyone has reacted, the instructions will be '
        'shown and then the game will start.'
    )
    await gg.msg_with_reaction.add_reaction('🐐')

    gg.state = 3
    # Return successful.
    return True


async def disp_instr(gg, text_channel):
    gg.state = 4

    await text_channel.send('Here\'s how the game works:')
    await asyncio.sleep(2)

    if gg.show_instr:
        await text_channel.send(
            f'There are {gg.the_game.rounds_per_game} rounds in total.'
        )
        await text_channel.send(
            f'In each round, each team will have to transmit a song '
            f'along a chain of {gg.the_game.chain_size} people.'
        )
        await text_channel.send(
            'At the start of each team\'s turn, '
            'all team members in the chain, except the first person in '
            'the chain (called the *listener*), will be moved to the '
            '*Earplugged 🎧* voice channel.'
        )
        await text_channel.send(
            'Next, the listener will be DM\'d the name of the song '
            'that they\'re about to listen to.'
        )
        await text_channel.send(
            'The listener will also be given access to a text channel called '
            '*top-secret-channel*. Here, the listener may use music bot '
            'commands without anyone else seeing, e.g. FredBoat\'s `;;play`, '
            '`;;pause`, `;;resume` etc.'
            '\n**Note to server admin:** FredBoat must have the *listener* '
            'role for it to access the *top-secret-channel*.'
        )
        await text_channel.send(
            'Once the listener has heard enough of the song, they should '
            'pause the music bot and then click the 🐐 react on the '
            'indicated message to proceed to the next stage of the game.'
        )
        await text_channel.send(
            'In the next stage, the bot will bring back each team member '
            'in the *Earplugged 🎧* VC, one by one. '
            'There will be a **20-second** pause '
            'after each team member is brought back to '
            'allow the previous team member to sing to that team member.'
        )
        await text_channel.send(
            'Finally, the last person in the chain must guess the song.'
        )
        await text_channel.send(
            'If the last person guesses incorrectly, then the '
            'second-to-last person must make a guess, and so on.'
        )
        await text_channel.send(
            'In each round, each team earns *n* - 1 points, where the '
            '*n*th person (*n* > 1) is the one closest to the end of '
            'the chain to correctly guess the song. If no-one gets it, '
            'then the team earns no points.'
        )
        await text_channel.send(
            'As of the latest version of Musical Whispers, the points that '
            'each team gets must be entered manually.'
        )

        gg.msg_with_reaction = await text_channel.send(
            'Once everyone is happy with the instructions, '
            'click the 🐐 react to begin the game!'
        )
        await gg.msg_with_reaction.add_reaction('🐐')
    else:
        await text_channel.send(
            'Oh wow, you nerds know how this works already.'
        )
        await asyncio.sleep(2)
        await text_channel.send('Well then, allons-y!')
        await asyncio.sleep(5)

        gg.state = 5
        await game_proper(gg, text_channel)


async def game_proper(gg, text_channel):
    print('About to enter the if statement™️')
    if gg.state == 5 and gg.current_team_num == len(gg.the_game.teams) - 1 \
            and gg.the_game.rounds_played == gg.the_game.rounds_per_game:
        # Finish the game.
        await text_channel.send(
            f'Round {gg.the_game.rounds_played} has finished.'
        )
        await text_channel.send('The game is now over.')
        await text_channel.send('Here\'s the leaderboard:')
        await disp_leaderboard(gg.the_game, text_channel)
        await text_channel.send('Thanks to all of you for playing!!!')
        await text_channel.send(
            'To play again, simply use the `$start` or `$wise_start` command. '
            'Any unused songs will have been saved, so you won\'t need to '
            'enter those again.'
        )
        gg.reset_vars(True)
    elif gg.state == 5:
        # Run a round (listening stage).
        print('About to run a listening stage')
        if gg.current_team_num == -1 \
                or gg.current_team_num == len(gg.the_game.teams) - 1:
            if gg.the_game.rounds_played > 0:
                await text_channel.send(
                    f'Round {gg.the_game.rounds_played} has finished. '
                    f'Here are the current standings:'
                )
                await disp_leaderboard(gg.the_game, text_channel)
            gg.current_team_num = 0
            gg.current_round_teams = gg.the_game.next_round()
        else:
            gg.current_team_num += 1

        gg.current_chain = gg.the_game.get_current_chain(gg.current_team_num)
        await text_channel.send(
            f'The following players from Team '
            f'{gg.current_round_teams[gg.current_team_num].team_name} '
            f'will play:'
        )
        await text_channel.send(
            ', '.join([m.member.nick if m.member.nick else m.member.name
                       for m in gg.current_chain])
        )
        await asyncio.sleep(3)

        for m in gg.current_chain[1:]:
            await m.member.move_to(gg.erplgd_channel)
        await asyncio.sleep(1)

        listener = gg.current_chain[0]
        guesser = gg.current_chain[-1]
        await listener.member.create_dm()
        await listener.member.dm_channel.send(
            f'You\'ll be listening to the song {guesser.current_song.title} '
            f'(url: {guesser.current_song.url}).'
            f'\nThe following FredBoat command can be used in the '
            f'*top-secret-channel*: `;;play {guesser.current_song.url}`'
        )

        try:
            await listener.member.add_roles(gg.role)
        except discord.Forbidden:
            await text_channel.send(
                'The bot doesn\'t have enough permissions to change a server '
                'member\'s roles. Please ensure that the '
                '\'*Musical Whispers*\' role is higher up than the *listener* '
                'role, then restart the game using the `$start` command.'
            )

        # Remove song from players.json file (if it hasn't been already).
        for song in all_player_data[str(guesser.member.id)]:
            if song['title'] == guesser.current_song.title:
                all_player_data[str(guesser.member.id)].remove(song)
                break
        update_json()

        gg.msg_with_reaction = await text_channel.send(
            f'{listener.member.mention}, once you\'re ready for your '
            f'teammates to be brought back, hit the 🐐 react!'
        )
        await gg.msg_with_reaction.add_reaction('🐐')

        gg.state = 6
    elif gg.state == 6:
        # Run a round (singing stage).
        print('About to run a singing stage')
        gg.state = 6.5

        listener = gg.current_chain[0]
        await listener.member.remove_roles(gg.role)

        guesser = gg.current_chain[-1]
        str_of_mentions = ', '.join([m.member.mention
                                     for m in gg.current_chain[1:]])
        await text_channel.send(f'{str_of_mentions}, get ready!')
        await asyncio.sleep(3)

        for i, m in enumerate(gg.current_chain[1:]):
            await m.member.move_to(gg.v_channel)
            await asyncio.sleep(20)

            await text_channel.send(
                f'{gg.current_chain[i].member.mention}, please stop singing!'
            )
            num = random.randint(1, 10)
            print(f'Trying to play {num}.mp3')
            gg.voice.stop()
            gg.voice.play(discord.FFmpegPCMAudio(f'sounds/{num}.mp3'))
            await asyncio.sleep(8)

        gg.msg_with_reaction = await text_channel.send(
            'When you\'re ready for the answer to be revealed, click the '
            '🐐 react.'
        )
        await gg.msg_with_reaction.add_reaction('🐐')

        gg.state = 7


async def add_points(gg, text_channel, response):
    try:
        points = int(response)
        print(f'Attempting to add {points} points '
              f'to team {gg.current_team_num}')
        gg.the_game.add_to_score(gg.current_team_num, points)
        gg.state = 5
        # Return successful.
        return True
    except (TypeError, ValueError):
        await text_channel.send('Invalid score :(\nTry again uwu')
        # Return unsuccessful.
        return False


async def disp_leaderboard(the_game, text_channel):
    leaderboard = the_game.get_leaderboard()
    numbering = ['' for _ in leaderboard]
    i = 0
    while i < len(leaderboard):
        # Figure out which teams are joint ranked with the current team.
        j = i + 1
        while j < len(leaderboard):
            if leaderboard[i].score != leaderboard[j].score:
                break
            j += 1

        # Set the prefix to '#' or '=' as appropriate.
        if j == i + 1:
            numbering[i] = f'#{i+1}'
        else:
            for k in range(i, j):
                numbering[k] = f'={i+1}'

        i = j

    for i, team in enumerate(leaderboard):
        await text_channel.send(
            f'{numbering[i]}: Team {team.team_name}, {team.score} points.'
        )


@bot.event
async def on_ready():
    print(f'{bot.user.name} has connected to Discord')


@bot.event
async def on_reaction_add(reaction, user):
    if user == bot.user:
        return

    gg = discord.utils.get(guild_games, guild=reaction.message.guild)
    if not gg:
        return

    print(f'Reaction added, state = {gg.state}')

    text_channel = reaction.message.channel

    if gg.state == 3 and reaction.message == gg.msg_with_reaction \
            and str(reaction) == '🐐':
        team_member = None
        for team_member in gg.the_game.all_members:
            if user == team_member.member:
                break

        if not team_member:
            return

        if team_member not in gg.members_ready:
            # Update the team member's Song objects
            # (if they've added >= 1 song).
            if str(team_member.member.id) not in all_player_data.keys() \
                    or not all_player_data[str(team_member.member.id)]:
                await reaction.message.reply(
                    f'{team_member.member.mention}, naughty naughty, '
                    f'you haven\'t actually entered any songs yet!'
                )
                await reaction.remove(user)
                return

            for song in all_player_data[str(team_member.member.id)]:
                team_member.add_song(game.Song(song['title'], song['url']))

            gg.members_ready.append(team_member)
            print(f'{team_member.member.name} is ready. '
                  f'{len(gg.the_game.all_members) - len(gg.members_ready)} '
                  f'players left to confirm')
            if len(gg.members_ready) == len(gg.the_game.all_members):
                await disp_instr(gg, text_channel)
    elif gg.state == 4 and reaction.message == gg.msg_with_reaction \
            and str(reaction) == '🐐':
        await text_channel.send('Well then, allons-y!')
        await asyncio.sleep(5)

        gg.state = 5
        await game_proper(gg, text_channel)
    elif gg.state == 6 and reaction.message == gg.msg_with_reaction \
            and str(reaction) == '🐐':
        await game_proper(gg, text_channel)
    elif gg.state == 7 and reaction.message == gg.msg_with_reaction \
            and str(reaction) == '🐐':
        guesser = gg.current_chain[-1]
        await text_channel.send(
            f'The correct answer is {guesser.current_song.title} '
            f'(url: {guesser.current_song.url}).'
        )
        await text_channel.send(
            f'When you\'re ready, enter the number of points that Team '
            f'{gg.current_round_teams[gg.current_team_num].team_name} '
            f'score this round.'
        )

        gg.state = 8

        def check(message):
            if any([message.content.startswith(cmd)
                    for cmd in ['$start', '$wise_start', '$abort']]):
                return True

            if message.channel != text_channel \
                    or message.author == bot.user:
                return False

            try:
                float(message.content)
                return True
            except ValueError:
                return False

        try:
            success = False
            while not success:
                message = await bot.wait_for('message', check=check,
                                             timeout=300)
                if any([message.content.startswith(cmd)
                        for cmd in ['$start', '$wise_start', '$abort']]):
                    # Abort.
                    gg.reset_vars(True)
                    return
                success = await add_points(gg, text_channel, message.content)
            await game_proper(gg, text_channel)
        except asyncio.TimeoutError:
            await text_channel.send('Timed out: game will now abort 🥺')
            gg.reset_vars(True)


@bot.command(
    name='start',
    help='Starts a new game and displays full instructions.'
)
async def start(ctx):
    await start_and_wait(ctx, True)


@bot.command(
    name='wise_start',
    help='Starts a new game and does *not* display the instructions.'
)
async def wise_start(ctx):
    await start_and_wait(ctx, False)


@bot.command(
    name='abort',
    help='Stops the current game.\n'
         'Useful if you want to break out of a never-ending loop!'
)
async def abort(ctx):
    gg = discord.utils.get(guild_games, guild=ctx.guild)
    if not gg:
        return

    await ctx.send('Game will now abort 🥺')
    gg.reset_vars(True)


@bot.command(
    name='add',
    help='Adds a new song or new songs to your list.\n\n'
         '*song_data* is the new song data and must be of the form '
         '<title (artist optional)> --- <url>.\n'
         'Multiple songs can be added at once as long as the song data '
         'are separated by linebreaks (Shift + Enter).\n\n'
         '**Example:**\n'
         '$add Bad Romance (Lady Gaga) --- '
         'https://www.youtube.com/watch?v=qrO4YZeyl0I\n\n'
         '$add Bad Romance (Lady Gaga) --- '
         'https://www.youtube.com/watch?v=qrO4YZeyl0I\n'
         'Replay (Lady Gaga) --- https://www.youtube.com/watch?v=ZTwpCLZLdRs'
)
async def add_songs(ctx, *, song_data: str):
    if isinstance(ctx.channel, discord.DMChannel):
        author = ctx.author

        if str(author.id) in all_player_data.keys():
            player_songs = all_player_data[str(author.id)]
        else:
            player_songs = []

        all_song_data = [s.split('---') for s in song_data.split('\n')]

        for song_data in all_song_data:
            if len(song_data) != 2:
                await ctx.send('Invalid formatting.')
                return

            song = {'title': song_data[0].strip(), 'url': song_data[1].strip()}
            player_songs.append(song)
            all_player_data[str(author.id)] = player_songs

            await ctx.send(
                f'The song {song_data[0].strip()} '
                f'(url: {song_data[1].strip()}) was added to your list.'
            )

        update_json()


@bot.command(
    name='list',
    help='Lists all your songs that are currently saved.'
)
async def list_songs(ctx):
    if isinstance(ctx.channel, discord.DMChannel):
        author = ctx.author

        if str(author.id) not in all_player_data.keys() \
                or not all_player_data[str(author.id)]:
            await ctx.send(
                'Looks like you don\'t have any songs currently '
                '¯\\\\\\_ (ツ)_/¯'
            )
        else:
            player_songs = all_player_data[str(author.id)]
            await ctx.send('Here are the songs you currently have:')

            song_str = ''
            for i, song in enumerate(player_songs):
                song_str += f'{i+1}. {song["title"]} --- {song["url"]}\n'
            await ctx.send(song_str)


@bot.command(
    name='remove',
    help='Removes songs from your list.\n\n'
         'The *response* must be a number corresponding to the song you want '
         'removed (as indicated by the $list command), or a list of numbers '
         'separated by commas.\n\n\n'
         '**Examples:**\n'
         '$remove 1\n'
         '$remove 6, 9, 4, 2'
)
async def remove_songs(ctx, *, response: str):
    if isinstance(ctx.channel, discord.DMChannel):
        author = ctx.author

        if str(author.id) not in all_player_data.keys() \
                or not all_player_data[str(author.id)]:
            await ctx.send(
                'Looks like you don\'t have any songs currently '
                '¯\\\\\\_ (ツ)_/¯'
            )
        else:
            try:
                nums = [int(s) for s in response.split(',')]
                player_songs = all_player_data[str(author.id)]
                player_songs_temp = player_songs.copy()

                for i, num in enumerate(nums):
                    if 1 <= num <= len(player_songs):
                        song_to_remove = player_songs[num - 1]
                        player_songs_temp.remove(song_to_remove)
                    else:
                        err_msg = f'Song number {num} doesn\'t exist!'
                        if i == 1:
                            err_msg += \
                                f'Song {nums[0]} *has* been removed though.'
                        elif i > 1:
                            err_msg += \
                                (f'\nThe songs numbered {str(nums[:i])[1:-1]} '
                                 f'*have* been removed though.')
                        raise IndexError(err_msg)

                if len(nums) > 1:
                    await ctx.send(
                        f'Songs {str(nums)[1:-1]} have been removed.'
                    )
                else:
                    await ctx.send(
                        f'Song {nums[0]} has been removed.'
                    )
            except ValueError:
                await ctx.send('Invalid formatting.')
                return
            except IndexError as ex:
                await ctx.send(ex)

            all_player_data[str(author.id)] = player_songs_temp
            update_json()


@bot.command(
    name='edit',
    help='Edits the title and/or url of an existing song.\n\n'
         '*num* is the number of the song as indicated by '
         'the $list command.\n\n'
         '*song_data* is the new song data and must be of the form '
         '<title (artist optional)> --- <url>.\n\n\n'
         '**Example:**\n'
         '$edit 2 Replay (Lady Gaga) --- '
         'https://www.youtube.com/watch?v=ZTwpCLZLdRs'
)
async def edit_song(ctx, num: int, *, song_data: str):
    if isinstance(ctx.channel, discord.DMChannel):
        author = ctx.author

        if str(author.id) not in all_player_data.keys() \
                or not all_player_data[str(author.id)]:
            await ctx.send(
                'Looks like you don\'t have any songs currently '
                '¯\\\\\\_ (ツ)_/¯'
            )
        else:
            try:
                player_songs = all_player_data[str(author.id)]

                if 1 <= num <= len(player_songs):
                    song_data = song_data.split('---')
                    if len(song_data) != 2:
                        await ctx.send('Invalid formatting.')
                        return
                    player_songs[num - 1] = {
                        'title': song_data[0].strip(),
                        'url': song_data[1].strip()
                    }
                else:
                    raise IndexError(f'Song number {num} doesn\'t exist!')

                await ctx.send(
                    f'Song {num} has been edited to {song_data[0].strip()} '
                    f'(url: {song_data[1].strip()}).'
                )
            except IndexError as ex:
                await ctx.send(ex)


@bot.command(
    name='fred',
    help='Tries to add the *listener* role (if it exists) to the FredBoat bot '
         '(if it exists).'
)
async def add_role_to_fredboat(ctx):
    fred = discord.utils.get(ctx.guild.members, id=184405311681986560)
    if not fred:
        await ctx.send(
            'FredBoat not found on this server :((\n'
            'If FredBoat has definitely been added to the server, then '
            'try summoning it to a voice channel first using `;;summon` '
            'and then running `$fred` again.'
        )
        return

    role = discord.utils.get(ctx.guild.roles, name='listener')
    if not role:
        await ctx.send(
            'The *listener* role must first be created in this server.'
        )
        return

    await fred.add_roles(role)
    await ctx.send('FredBoat upgrade complete!')


if __name__ == '__main__':
    bot.run(TOKEN)
